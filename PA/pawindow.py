# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc


###########################################################################
## Class PAzhihu
###########################################################################

class PAzhihu(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=u"从此斗图不用愁", pos=wx.DefaultPosition, size=wx.Size(500, 200),
                          style=wx.DEFAULT_FRAME_STYLE | wx.TAB_TRAVERSAL)

        self.icon = wx.Icon('yeah.ico', wx.BITMAP_TYPE_ICO)

        self.SetIcon(self.icon)

        self.SetBackgroundColour('#00FFFF')

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        all = wx.BoxSizer(wx.VERTICAL)

        urlANDos = wx.BoxSizer(wx.VERTICAL)

        url = wx.BoxSizer(wx.HORIZONTAL)

        self.URL = wx.StaticText(self, wx.ID_ANY, u"输入链接：", wx.DefaultPosition, wx.DefaultSize, 0)
        self.URL.Wrap(-1)

        url.Add(self.URL, 0, wx.ALL, 5)

        self.url_input = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        url.Add(self.url_input, 1, wx.ALL, 5)

        urlANDos.Add(url, 1, wx.EXPAND, 5)

        os = wx.BoxSizer(wx.HORIZONTAL)

        self.ospath = wx.TextCtrl(self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
        os.Add(self.ospath, 1, wx.ALL, 5)

        self.select = wx.Button(self, wx.ID_ANY, u"选择保存路径", wx.DefaultPosition, wx.DefaultSize, 0)
        os.Add(self.select, 0, wx.ALL, 5)

        self.tosave = wx.Button(self, wx.ID_ANY, u"开始", wx.DefaultPosition, wx.DefaultSize, 0)
        os.Add(self.tosave, 0, wx.ALL, 5)

        urlANDos.Add(os, 1, wx.EXPAND, 5)

        tip = wx.BoxSizer(wx.VERTICAL)

        self.tips = wx.StaticText(self, wx.ID_ANY, u"点击“开始”开始爬取表情\n点右上角“X”退出", wx.DefaultPosition, wx.DefaultSize,
                                  wx.ALIGN_CENTER_HORIZONTAL)
        self.tips.Wrap(-1)

        tip.Add(self.tips, 1, wx.ALL | wx.EXPAND, 5)

        urlANDos.Add(tip, 1, wx.EXPAND, 5)

        all.Add(urlANDos, 1, wx.EXPAND, 5)

        self.SetSizer(all)
        self.Layout()

        self.Centre(wx.BOTH)

        # Connect Events
        self.select.Bind(wx.EVT_BUTTON, self.select_file)
        self.tosave.Bind(wx.EVT_BUTTON, self.save_images)

    def __del__(self):
        pass

        # Virtual event handlers, overide them in your derived class

    def select_file(self, event):
        event.Skip()

    def save_images(self, event):
        event.Skip()