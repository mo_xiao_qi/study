# 这是一个示例 Python 脚本。

# 按 Shift+F10 执行或将其替换为您的代码。
# 按 双击 Shift 在所有地方搜索类、文件、工具窗口、操作和设置。
# import tkinter as tk


import tkinter as tk

# @from tkinter.constants import *
dictionary = {'a': '10', 'b': '0111', 'c': '0101', 'd': '011', 'e': '1', 'f': '1101',
              'g': '001', 'h': '1111', 'i': '11', 'j': '1000', 'k': '010', 'l': '1011',
              'm': '00', 'n': '01', 'o': '000', 'p': '1001', 'q': '0010', 'r': '101',
              's': '111', 't': '0', 'u': '110', 'v': '1110', 'w': '100', 'x': '0110',
              'y': '0100', 'z': '0011'}
window = tk.Tk()
window.title('window')
window.geometry("400x200")
e = tk.Entry(window)
e.place(x=0, y=160, height=40, width=120)


def insert_button():
    var = e.get()
    for i in var:
        e.delete(0, 'end')
        t1.insert('insert', dictionary[i] + ' ')
    t1.insert('insert', '\n')


def enter(self):
    insert_button()


b1 = tk.Button(window, text='send', height=1, command=insert_button)
b1.place(x=120, y=160, height=40, width=80)
e.bind("<Return>", enter)

t1 = tk.Text(window)
t1.place(x=0, y=0, height=160, width=200)

t2 = tk.Text(window)
t2.place(x=200, y=0, height=160, width=200)

window.mainloop()
# 访问 https://www.jetbrains.com/help/pycharm/ 获取 PyCharm 帮助
