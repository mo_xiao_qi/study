import wx
import clientwindow
import os
import os.path
import socket
import base64
client=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
client.connect(('localhost',2613))
content=client.recv(2048)
class CliFrame(clientwindow.Client):
    def __init__(self, parent):
        clientwindow.Client.__init__(self, parent)
        self.file_content_e.SetValue(content)
    def decrypt_con(self,event):
        content_d=base64.b64decode(content)
        self.file_content_d.SetValue(content_d)
    def save_con(self,event):
        text=self.file_name.GetValue()
        file=open(text,"wb+")
        content_d=base64.b64decode(content)
        file.write(content_d)
        file.close()

def main():
    app = wx.App(False)
    frame = CliFrame(None)
    frame.Show(True)
    app.MainLoop()

if __name__ == "__main__":
    main()
    pass
