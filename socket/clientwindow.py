# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class Client
###########################################################################

class Client ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u" Client", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.icon = wx.Icon('client.ico', wx.BITMAP_TYPE_ICO)

		self.SetIcon(self.icon)

		self.SetBackgroundColour('#00FFFF')

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer1 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer2 = wx.BoxSizer( wx.VERTICAL )

		self.file_content_e = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, style=wx.TE_MULTILINE )
		bSizer2.Add( self.file_content_e, 1, wx.ALL|wx.EXPAND, 5 )

		self.decrypt = wx.Button( self, wx.ID_ANY, u"解密", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer2.Add( self.decrypt, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer1.Add( bSizer2, 1, wx.EXPAND, 5 )

		bSizer3 = wx.BoxSizer( wx.VERTICAL )

		bSizer4 = wx.BoxSizer( wx.VERTICAL )

		self.file_content_d = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, style=wx.TE_MULTILINE )
		bSizer4.Add( self.file_content_d, 1, wx.ALL|wx.EXPAND, 5 )


		bSizer3.Add( bSizer4, 9, wx.EXPAND, 5 )

		bSizer6 = wx.BoxSizer( wx.HORIZONTAL )

		self.file_name = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer6.Add( self.file_name, 0, wx.ALL, 5 )

		self.save = wx.Button( self, wx.ID_ANY, u"保存", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer6.Add( self.save, 0, wx.ALL, 5 )


		bSizer3.Add( bSizer6, 1, wx.EXPAND, 5 )


		bSizer1.Add( bSizer3, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.decrypt.Bind( wx.EVT_BUTTON, self.decrypt_con )
		self.save.Bind( wx.EVT_BUTTON, self.save_con )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def decrypt_con( self, event ):
		event.Skip()

	def save_con( self, event ):
		event.Skip()


