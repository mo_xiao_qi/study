import wx
import severwindow
import socket
import os
import os.path
import base64
sever=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
sever.bind(('localhost',2613))
sever.listen()
conn,addr=sever.accept()
class CliFrame(severwindow.Sever):
    def __init__(self, parent):
        severwindow.Sever.__init__(self, parent)
    def open_file(self,event):
        text=self.file_name.GetValue()
        file=open(text,"rb+")
        self.file_content_d.SetValue(file.read())
        file.close()
    def encrypt_con(self,event):
        content_d=self.file_content_d.GetValue()
        content_e=base64.b64encode(content_d.encode())
        self.file_content_e.SetValue(content_e)
    def send_file(self,event):
        content=self.file_content_e.GetValue()
        conn.send(content.encode())
def main():
    app = wx.App(False)
    frame = CliFrame(None)
    frame.Show(True)
    app.MainLoop()

if __name__ == "__main__":
    main()
    pass
