# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class Sever
###########################################################################

class Sever ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Sever", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.icon = wx.Icon('sever.ico', wx.BITMAP_TYPE_ICO)

		self.SetIcon(self.icon)

		self.SetBackgroundColour('#00FFFF')

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer0 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		bSizer3 = wx.BoxSizer( wx.HORIZONTAL )

		self.file_name = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer3.Add( self.file_name, 0, wx.ALL, 5 )

		self.open = wx.Button( self, wx.ID_ANY, u"打开", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer3.Add( self.open, 0, wx.ALL, 5 )


		bSizer1.Add( bSizer3, 1, wx.EXPAND, 5 )

		bSizer4 = wx.BoxSizer( wx.VERTICAL )

		self.file_content_d = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, style=wx.TE_MULTILINE )
		bSizer4.Add( self.file_content_d, 1, wx.ALL|wx.EXPAND, 5 )

		self.encrtpt = wx.Button( self, wx.ID_ANY, u"加密", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer4.Add( self.encrtpt, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer1.Add( bSizer4, 9, wx.EXPAND, 5 )


		bSizer0.Add( bSizer1, 1, wx.EXPAND, 5 )

		bSizer2 = wx.BoxSizer( wx.VERTICAL )

		self.file_content_e = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, style=wx.TE_MULTILINE )
		bSizer2.Add( self.file_content_e, 1, wx.ALL|wx.EXPAND, 5 )

		self.send = wx.Button( self, wx.ID_ANY, u"发送", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer2.Add( self.send, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )


		bSizer0.Add( bSizer2, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer0 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.open.Bind( wx.EVT_BUTTON, self.open_file )
		self.encrtpt.Bind( wx.EVT_BUTTON, self.encrypt_con )
		self.send.Bind( wx.EVT_BUTTON, self.send_file )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def open_file( self, event ):
		event.Skip()

	def encrypt_con( self, event ):
		event.Skip()

	def send_file( self, event ):
		event.Skip()


