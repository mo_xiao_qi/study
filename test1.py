# # from itertools import zip_longest
# #
# # encode_dictionary = {'a': '10', 'b': '0111', 'c': '0101', 'd': '011', 'e': '1', 'f': '1101',
# #                      'g': '001', 'h': '1111', 'i': '11', 'j': '1000', 'k': '010', 'l': '1011',
# #                      'm': '00', 'n': '01', 'o': '000', 'p': '1001', 'q': '0010', 'r': '101',
# #                      's': '111', 't': '0', 'u': '110', 'v': '1110', 'w': '100', 'x': '0110',
# #                      'y': '0100', 'z': '0011', '1': '10000', '2': '11000', '3': '11100',
# #                      '4': '11110', '5': '11111', '6': '01111', '7': '00111', '8': '000110',
# #                      '9': '00001', '0': '00000', '.': '101010', ',': '001100', ':': '000111',
# #                      '?': '110011', "'": '100001', '-': '011110', '/': '01101', '(': '01001',
# #                      ')': '010010', '"': '101101', '=': '01110', '+': '10101', '@': '100101',
# #                      '!': '010100', '&': '10111'}
# #
# # decode_dictionary = dict(zip(encode_dictionary.values(), encode_dictionary.keys()))
# # # print(decode_dictionary)
# #
# # ins = input()
# # str1 = ''
# # for i in ins:
# #     if i == ' ':
# #         str1 = str1 + ' '
# #     else:
# #         str1 = str1 + encode_dictionary[i] + ' '
# #
# # # print(str1)
# # list1 = str1.split('  ')
# # str2 = ''
# # for j in list1:
# #     i = j.split()
# #     for k in i:
# #         str2 = str2 + decode_dictionary[k]
# #     str2 = str2 + ' '
# # print(str2)
#
#
# for i in range(1, 1000):
#     if i % 3 == 2 and i % 5 == 3 and i % 7 == 2:
#         print(i)


import sys
import tty
import termios


def readchar():
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch


def readkey(getchar_fn=None):
    getchar = getchar_fn or readchar
    c1 = getchar()
    if ord(c1) != 0x1b:
        return c1
    c2 = getchar()
    if ord(c2) != 0x5b:
        return c1
    c3 = getchar()
    return chr(0x10 + ord(c3) - 65)


while True:
    key = readkey()
    if key == 'w':
        print('w')
    if key == 'a':
        print('a')
    if key == 's':
        print('s')
    if key == 'd':
        print('d')
    if key == 'q':
        break