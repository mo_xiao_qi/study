#include<pthread.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
void * func(void * arg)
{
 for(int i = 0;i < 10;i++) printf("i=%d\n",i);
 return NULL;
}
int main()
{
 pthread_t t1;
 int err = pthread_create(&t1,NULL,func,NULL);
 if(err!=0)
 {
  printf("thread_create Failed:%s\n",strerror(errno));
 }else{
  printf("thread_create success\n");
 }
 for(int j = 0;j < 10;j++) printf("j=%d\n",j);
 return EXIT_SUCCESS;
}