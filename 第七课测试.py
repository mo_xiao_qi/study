'''
作者：20183215董振龙
文件名称：第七课测试.py
时间：2020年4月8日20点42分
'''
class Student:
    __name="董振龙"
    _grade="大二"
    def __init__(self,name,grade):
        self.name=name
        self.grade=grade
class Subject(Student):
    def __init__(self, name, grade, subject):
        super().__init__(name, grade)
        self.subject=subject
    def pri(self):
        print("%s在%s时有%s课"%(self.name,self.grade,self.subject))
class Time(Subject):
    def __init__(self,time):
        self.time=time
    def __add__(self, other):
        return Time(self.time+other.time)
    def pri2(self):
        print("共有%d课时"%(self.time))
S=Subject("董振龙","大二下","Python程序语言设计")
S.pri()
T1=Time(20)
T2=Time(30)
T=T1+T2
T.pri2()
