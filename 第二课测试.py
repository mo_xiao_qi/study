'''
作者：20183215董振龙
文件名称：第二课测试.py
时间：2020年3月4日18点55分
'''
test1 = '''1、已知：E(x)=(5x+8) mod 26函数，解密函数就是D(x)=21(x-8) mod 26。
要求：输入一个x，计算E(x)的值y。然后计算D(y)并判断x是否等于D(y)。'''
print(test1)
a = int(input("输入x的值："))
y = (5*a+8)%26
x = 21*(y-8)%26
print("y=E(x)=",y)
print("D(y)=",21*(y-8)%26)
if a==x:
    print("x等于D(y)")
test2 = '''2、等比数列的公式：
设数列{a×q^(n-1)}是首项为a,公比为q的等比数列.即a,aq,aq²,aq³,...aq^(n-1).(n=1,2,3,4...)，其前n项和为Sn。
当q=1时,Sn=na.(n=1,2,3,.)
当q≠1时,Sn=a[(q^n)-1]/(q-1) (n=1,2,3,...)
输入3个数a，n，q（q≠1），求Sn'''
print(test2)
a = int(input("a:"))
n = int(input("n:"))
q = int(input("q(q≠1):"))
print("Sn=",int(a*((q**n)-1)/(q-1)))