'''
作者：20183215董振龙
文件名称：第四课测试.py
时间：2020年3月21日15点56分
'''
tank = ["张飞","吕布","孙策","庄周","芈月","夏侯惇","项羽","凯"] # 坦克
warrior = ["吕布","关羽","孙策","墨子","夏侯惇","赵云","橘右京","凯"] # 战士
assassin = ["不知火舞","百里守约","貂蝉","上官婉儿","韩信","李白","元歌","赵云"] # 刺客
mage = ["不知火舞","干将莫邪","孙膑","诸葛亮","貂蝉","嬴政","上官婉儿","芈月"] # 法师
shooter = ["百里守约","鲁班七号","孙尚香","马可波罗","后羿","李元芳","狄仁杰","黄忠"] # 射手
auxiliary = ["明世隐","张飞","孙膑","大乔","庄周","瑶","蔡文姬","盾山"] # 辅助
all = [tank,warrior,assassin,mage,shooter,auxiliary]
name = ["===坦克===","===战士===","===刺客===","===法师===","===射手===","===辅助==="]
for i in range(5):
    print(name[i])
    print(all[i])
if input("是否要进行修改、添加或删除?\n") == "是":
    print("请输入要修改英雄所属类型：\n1 坦克\n2 战士\n3 刺客\n4 法师\n5 射手\n6 辅助")
    j=input()
    if j =="1":
        print("===坦克===\n",tank)
        k=input("要进行的操作是：")
        if k =="修改":
            xiu = input("要修改的英雄名称是：")
            tank.remove(xiu)
            gai = input("要改成的英雄名称是：")
            tank.append(gai)
            print("===坦克===(修改后)\n",tank)
        elif k =="添加":
            jia = input("要添加的英雄名称是：")
            tank.append(jia)
            print("===坦克===(添加后)\n",tank)
        elif k =="删除":
            shan = input("要删除的英雄名称是：")
            tank.remove(shan)
            print("===坦克===(删除后)\n",tank)
    elif j =="2":
        print("===战士===\n", warrior)
        k = input("要进行的操作是：")
        if k == "修改":
            xiu = input("要修改的英雄名称是：")
            warrior.remove(xiu)
            gai = input("要改成的英雄名称是：")
            warrior.append(gai)
            print("===战士===(修改后)\n", warrior)
        elif k == "添加":
            jia = input("要添加的英雄名称是：")
            warrior.append(jia)
            print("===战士===(添加后)\n", warrior)
        elif k == "删除":
            shan = input("要删除的英雄名称是：")
            warrior.remove(shan)
            print("===战士===(删除后)\n", warrior)
    elif j =="3":
        print("===刺客===\n", assassin)
        k = input("要进行的操作是：")
        if k == "修改":
            xiu = input("要修改的英雄名称是：")
            assassin.remove(xiu)
            gai = input("要改成的英雄名称是：")
            assassin.append(gai)
            print("===刺客===(修改后)\n", assassin)
        elif k == "添加":
            jia = input("要添加的英雄名称是：")
            assassin.append(jia)
            print("===刺客===(添加后)\n", assassin)
        elif k == "删除":
            shan = input("要删除的英雄名称是：")
            assassin.remove(shan)
            print("===刺客===(删除后)\n", assassin)
    elif j =="4":
        print("===法师===\n", mage)
        k = input("要进行的操作是：")
        if k == "修改":
            xiu = input("要修改的英雄名称是：")
            mage.remove(xiu)
            gai = input("要改成的英雄名称是：")
            mage.append(gai)
            print("===法师===(修改后)\n", mage)
        elif k == "添加":
            jia = input("要添加的英雄名称是：")
            mage.append(jia)
            print("===法师===(添加后)\n", mage)
        elif k == "删除":
            shan = input("要删除的英雄名称是：")
            mage.remove(shan)
            print("===法师===(删除后)\n", mage)
    elif j =="5":
        print("===射手===\n", shooter)
        k = input("要进行的操作是：")
        if k == "修改":
            xiu = input("要修改的英雄名称是：")
            shooter.remove(xiu)
            gai = input("要改成的英雄名称是：")
            shooter.append(gai)
            print("===射手===(修改后)\n", shooter)
        elif k == "添加":
            jia = input("要添加的英雄名称是：")
            shooter.append(jia)
            print("===射手===(添加后)\n", shooter)
        elif k == "删除":
            shan = input("要删除的英雄名称是：")
            shooter.remove(shan)
            print("===射手===(删除后)\n",shooter)
    else:
        print("===辅助===\n", auxiliary)
        k = input("要进行的操作是：")
        if k == "修改":
            xiu = input("要修改的英雄名称是：")
            auxiliary.remove(xiu)
            gai = input("要改成的英雄名称是：")
            auxiliary.append(gai)
            print("===辅助===(修改后)\n", auxiliary)
        elif k == "添加":
            jia = input("要添加的英雄名称是：")
            auxiliary.append(jia)
            print("===辅助===(添加后)\n", auxiliary)
        elif k == "删除":
            shan = input("要删除的英雄名称是：")
            auxiliary.remove(shan)
            print("===辅助===(删除后)\n", auxiliary)
