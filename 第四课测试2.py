'''
作者：20183215董振龙
文件名称：第四课测试2.py
时间：2020年3月21日16点02分
'''
def takeSecond(elem):
    return elem[1]
Unnatural = ("非自然死亡",170262)
Heartbeat = ("心动讯号",237607)
My_head_my_head = ("我的团长我的团",156213)
The_Road_to_Taste = ("绝味之路",162909)
Iron_tooth_copper_tooth_Ji_Xiaolan = ("铁齿铜牙纪晓岚",127635)
Monkey_King = ("天地争霸美猴王",99560)
Strange_House = ("怪味屋",184168)
Champion_and_me = ("冠军与我",285011)
Tokyo_Grand_Hotel = ("东京大饭店",675179)
Collective_demotion = ("集体降职",636600)
bilibili =[Unnatural,Heartbeat,My_head_my_head,The_Road_to_Taste,Iron_tooth_copper_tooth_Ji_Xiaolan,
           Monkey_King,Strange_House,Champion_and_me,Tokyo_Grand_Hotel,Collective_demotion]
bilibili.sort(key=takeSecond,reverse=True)
tplt = "{0:^10}\t{1:^10}\t"
print("     名字         综合得分")
for i in range(10):
    print(tplt.format(bilibili[i][0],bilibili[i][1]))
I = input("是否要进行修改、删除或添加：")
if I == "是":
    J = input("要进行的操作是：")
    if J == "修改":
        xiu = input("要修改的剧名是：")
        for j in range(10):
            if (xiu in bilibili[j]) is True:
                bilibili.remove(bilibili[j])
                print("输入修改后的剧名及综合得分：")
                gai1 = input("剧名：")
                gai2 = int(input("综合得分："))
                gai = (gai1,gai2)
                bilibili.append(gai)
                bilibili.sort(key=takeSecond, reverse=True)
                print("修改后如下：")
                tplt = "{0:^10}\t{1:^10}\t"
                print("     名字         综合得分")
                for i in range(10):
                    print(tplt.format(bilibili[i][0], bilibili[i][1]))
                break
    elif J == "删除":
        shan = input("要删除的剧名是：")
        for j in range(10):
            if (shan in bilibili[j]) is True:
                bilibili.remove(bilibili[j])
                bilibili.sort(key=takeSecond, reverse=True)
                print("删除后如下：")
                tplt = "{0:^10}\t{1:^10}\t"
                print("     名字         综合得分")
                for i in range(9):
                    print(tplt.format(bilibili[i][0], bilibili[i][1]))
                break
    else:
        print("输入要添加的剧名及综合得分：")
        zeng1 = input("剧名：")
        zeng2 = int(input("综合得分："))
        zeng = (zeng1, zeng2)
        bilibili.append(zeng)
        bilibili.sort(key=takeSecond, reverse=True)
        print("添加后如下：")
        tplt = "{0:^10}\t{1:^10}\t"
        print("     名字         综合得分")
        for i in range(11):
            print(tplt.format(bilibili[i][0], bilibili[i][1]))

