import math
def add(a,b): #加法函数
    print(a,"+",b,"=",a+b)
    return a+b
def sub(a,b): #减法函数
    print(a,"-",b,"=",a-b)
    return a-b
def mul(a,b): #乘法函数
    print(a,"×",b,"=",a*b)
    return a*b
def div(a,b): #除法函数
    if b==0:
        b=float(input("除数为零，请重新输入："))
        print(a,"÷",b,"=",a/b)
        return a/b
    else:
        print(a,"÷",b,"=",a/b)
        return a / b
def mod(a,b): #模运算
    if b==0:
        b=float(input("除数为零，请重新输入："))
        print(a,"Mod",b,"=",a%b)
        return a%b
    else:
        print(a,"Mod",b,"=",a%b)
        return a%b
def log(a,b): #普通对数运算函数
    print("log(a,b)=",math.log(a,b))
    return math.log(a,b)
def exp(a,b): #幂运算
    print(a,"^",b,"=",a**b)
def lg(a): #lg运算函数
    print("lg(a)=",math.log(a,10))
    return math.log(a,10)
def ln(a): #ln运算函数
    print("ln(a)=",math.log(a))
    return math.log(a)
def exp_e(a): #e的n次方
    print("e^",a,"=",math.e**a)
def sin(a): #sin函数
    print("sin",str(a)+"°","=",math.sin(math.radians(a)))
    return math.sin(math.radians(a))
def cos(a): #cos函数
    print("cos",str(a)+"°","=",math.cos(math.radians(a)))
    return math.cos(math.radians(a))
def tan(a): #tan函数
    if math.radians(a)==math.pi/2:
        a=float(input("不可以为Π/2，请重新输入："))
        print("tan", str(a) + "°", "=", math.tan(math.radians(a)))
        return math.tan(math.radians(a))
    else:
        print("tan", str(a) + "°", "=", math.tan(math.radians(a)))
        return math.tan(math.radians(a))
i=1
while(i==1):
    if(input("1、单数运算\n"
             "2、多数运算(若为对数运算，第二个数为底数；若为幂运算，第二个数为指数)\n")=="2"):
        a = float(input("第一个数："))
        b = float(input("第二个数："))
        c =int(input("运算操作为：\n"
                "1.+\n"
                "2.-\n"
                "3.×\n"
                "4.÷\n"
                "5.Mod\n"
                "6.log\n"
                "7.exp\n"))
        if c==1:
            add(a,b)
        elif c==2:
            sub(a,b)
        elif c==3:
            mul(a,b)
        elif c==4:
            div(a,b)
        elif c==5:
            mod(a,b)
        elif c==6:
            log(a,b)
        elif c==7:
            exp(a,b)
    else:
        a=float(input("请输入数值："))
        c=int(input("运算操作为：\n"
                "1.sin\n"
                "2.cos\n"
                "3.tan\n"
                "4.lg\n"
                "5.ln\n"
                "6.e^\n"))
        if c==1:
            sin(a)
        elif c==2:
            cos(a)
        elif c==3:
            tan(a)
        elif c==4:
            lg(a)
        elif c==5:
            ln(a)
        elif c==6:
            exp_e(a)
    i=int(input("继续进行计算？\n"
                "1.是\n"
                "2.否\n"))
